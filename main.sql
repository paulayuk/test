-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Sep 24, 2019 at 10:32 AM
-- Server version: 5.7.25
-- PHP Version: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `loanbot`
--

-- --------------------------------------------------------

--
-- Table structure for table `analyses`
--

CREATE TABLE `analyses` (
  `id` int(10) UNSIGNED NOT NULL,
  `total_credit` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total_debit` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `minimum_balance` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `maximum_balance` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `average_balance` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `opening_balance` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `closing_balance` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `categories` text COLLATE utf8mb4_unicode_ci,
  `credit_transfer` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `debit_transfer` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pos` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `charges` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `repayment` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `salary` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gambling` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `second_acc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `all_debits` text COLLATE utf8mb4_unicode_ci,
  `all_credit` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `salary_consistency` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `banks`
--

CREATE TABLE `banks` (
  `id` int(11) NOT NULL,
  `bank_name` varchar(255) NOT NULL,
  `bank_id` varchar(255) NOT NULL,
  `bank_code` varchar(20) DEFAULT NULL,
  `active` int(11) NOT NULL,
  `pdf` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `banks`
--

INSERT INTO `banks` (`id`, `bank_name`, `bank_id`, `bank_code`, `active`, `pdf`) VALUES
(1, 'Sterling Bank', '1', 'sterling', 1, 1),
(2, 'Polaris Bank', '2', 'polaris', 1, NULL),
(3, 'Access Bank', '6', 'access', 1, NULL),
(4, 'Heritage Bank', '7', 'heritage', 1, NULL),
(5, 'Diamond Bank', '8', 'diamond', 1, 0),
(6, 'Union Bank', '11', 'union', 1, 1),
(7, 'UBA', '14', 'uba', 1, 1),
(8, 'Fidelity Bank', '15', 'fidelity', 1, NULL),
(9, 'GTB', '13', 'gtb', 0, 1),
(30, 'FCMB', '16', 'fcmb', 0, 1),
(31, 'First Bank', '17', 'firstbank', 0, 1),
(32, 'Citi Bank', '18', 'citi', 0, NULL),
(33, 'EcoBank', '19', 'ecobank', 0, 1),
(34, 'Zenith Bank', '20', 'zenith', 0, 0),
(35, 'Keystone Bank', '21', 'keystone', 0, NULL),
(36, 'Stanbic IBTC', '22', 'stanbic', 0, NULL),
(37, 'Standard Chartered', '23', 'scb', 0, 1),
(38, 'Unity Bank', '24', 'unity', 0, NULL),
(39, 'Wema Bank', '12', 'wema', 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `category_keyword`
--

CREATE TABLE `category_keyword` (
  `id` int(11) NOT NULL,
  `keyword` varchar(300) NOT NULL,
  `category_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `crc_credit_rating`
--

CREATE TABLE `crc_credit_rating` (
  `id` int(11) NOT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `credit_score` int(4) DEFAULT NULL,
  `credit_rating` varchar(45) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `crc_report`
--

CREATE TABLE `crc_report` (
  `id` int(11) NOT NULL,
  `customer_id` varchar(45) DEFAULT NULL,
  `provider_source` varchar(200) DEFAULT NULL,
  `facilities_count` int(11) DEFAULT NULL,
  `performing_facility` int(11) DEFAULT NULL,
  `non_performing` int(11) DEFAULT NULL,
  `approved_amount` varchar(100) DEFAULT NULL,
  `account_balance` varchar(45) DEFAULT NULL,
  `bureau_currency` varchar(45) DEFAULT NULL,
  `overdue_amount` varchar(100) DEFAULT NULL,
  `dishonored_cheques` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `creditreport`
--

CREATE TABLE `creditreport` (
  `c_id` int(11) NOT NULL,
  `c_DataTicket` varchar(1000) DEFAULT NULL,
  `c_bvn` varchar(20) NOT NULL,
  `c_EnquiryReason` varchar(100) DEFAULT NULL,
  `c_ConsumerName` varchar(200) DEFAULT NULL,
  `c_DateOfBirth` varchar(100) DEFAULT NULL,
  `c_Identification` varchar(100) DEFAULT NULL,
  `c_AccountNumber` varchar(20) DEFAULT NULL,
  `c_ProductID` int(11) DEFAULT NULL,
  `c_ConsumerID` int(11) DEFAULT NULL,
  `c_FirstName` varchar(100) DEFAULT NULL,
  `c_Surname` varchar(100) DEFAULT NULL,
  `c_OtherNames` varchar(100) DEFAULT NULL,
  `c_Address` varchar(100) DEFAULT NULL,
  `c_BirthDate` varchar(100) DEFAULT NULL,
  `c_AccountNo` varchar(100) DEFAULT NULL,
  `c_DriversLicenseNo` varchar(100) DEFAULT NULL,
  `c_VoterID` varchar(100) DEFAULT NULL,
  `c_user_id` int(11) DEFAULT NULL,
  `c_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `c_status` int(11) DEFAULT NULL,
  `c_bureau` int(11) DEFAULT NULL,
  `c_EnquiryEngineID` int(11) DEFAULT NULL,
  `c_enquiryID` int(11) DEFAULT NULL,
  `c_consumerMergeList` varchar(200) DEFAULT NULL,
  `c_MatchingEngineID` varchar(20) DEFAULT NULL,
  `c_Reference` varchar(100) DEFAULT NULL,
  `c_MatchingRate` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `creditreportraw`
--

CREATE TABLE `creditreportraw` (
  `cr_id` int(11) NOT NULL,
  `c_id` int(11) NOT NULL,
  `c_match` text NOT NULL,
  `c_fullreport` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `credit_limit`
--

CREATE TABLE `credit_limit` (
  `id` int(10) UNSIGNED NOT NULL,
  `salary` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `loan_tenure` int(11) DEFAULT NULL,
  `emp_status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `customer_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bvn` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `statement_path` text COLLATE utf8mb4_unicode_ci,
  `response_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bank` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `credit_limit` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` int(11) NOT NULL,
  `bvn` varchar(11) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `bank_id` varchar(45) DEFAULT NULL,
  `first_name` varchar(45) DEFAULT NULL,
  `last_name` varchar(45) DEFAULT NULL,
  `account_number` varchar(45) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` varchar(45) DEFAULT NULL,
  `date_of_birth` varchar(45) DEFAULT NULL,
  `creditLimit` decimal(10,2) DEFAULT NULL,
  `pdf_statement` varchar(200) DEFAULT NULL,
  `report_id` varchar(45) DEFAULT NULL,
  `group` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `customer_reports`
--

CREATE TABLE `customer_reports` (
  `id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `started_by` int(11) DEFAULT NULL,
  `completed_by` int(11) DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `statement_number` int(11) DEFAULT NULL,
  `analysis_id` varchar(200) DEFAULT NULL,
  `loan_tenure` int(11) DEFAULT NULL,
  `csv_path` varchar(45) DEFAULT NULL,
  `bank_code` varchar(45) DEFAULT NULL,
  `comment` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `queue` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `attempts` tinyint(3) UNSIGNED NOT NULL,
  `reserved_at` int(10) UNSIGNED DEFAULT NULL,
  `available_at` int(10) UNSIGNED NOT NULL,
  `created_at` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(11) NOT NULL,
  `rate_3` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `statements`
--

CREATE TABLE `statements` (
  `id` int(11) NOT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `transaction_date` varchar(255) DEFAULT NULL,
  `debit` varchar(255) DEFAULT NULL,
  `credit` varchar(255) DEFAULT NULL,
  `balance` varchar(45) DEFAULT NULL,
  `reference` text,
  `remarks` text,
  `created_at` varchar(255) DEFAULT NULL,
  `updated_at` varchar(255) DEFAULT NULL,
  `category` varchar(255) DEFAULT NULL,
  `report_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `statement_category`
--

CREATE TABLE `statement_category` (
  `sc_id` int(11) NOT NULL,
  `sc_name` varchar(100) NOT NULL,
  `sc_parent` int(11) NOT NULL DEFAULT '0',
  `sc_status` int(11) NOT NULL DEFAULT '0',
  `sc_color` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `statement_samples`
--

CREATE TABLE `statement_samples` (
  `id` int(11) NOT NULL,
  `bank_name` varchar(45) DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `code` varchar(45) DEFAULT NULL,
  `sample` varchar(45) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `statement_summaries`
--

CREATE TABLE `statement_summaries` (
  `id` int(10) UNSIGNED NOT NULL,
  `customer_id` int(11) NOT NULL,
  `total_credit` double(8,2) NOT NULL,
  `total_debit` double(8,2) NOT NULL,
  `minimum_balance` double(8,2) NOT NULL,
  `maximum_balance` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `average_balance` double(8,2) NOT NULL,
  `opening_balance` double(8,2) NOT NULL,
  `closing_bal` double(8,2) NOT NULL,
  `report_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Dare Adewuyi', 'fehintolu@admin.com', NULL, '$2y$10$A3Rt9fVvV.57day0zBNF1OTje6cHv4qzw1H1HWIMyZ9E7VZ49ohpO', 'enBZupHsFHMRsUfnbeDEoNpSY5cR0Oc4YVc86cAXxkrt0E1gZmxRYzcRA3XD', '2019-01-30 10:49:33', '2019-01-30 10:49:33'),
(2, 'Yinka', 'yinka@me.com', NULL, '$2y$10$2YN6J/469qlARfKhBpCu8ONdZ/lre.atxscWzvbofzWJ1K0U/YoX.', 'Rl7bpgoPSDUdGqObfk2c5An6u5ORZhNIA4JWQkm9TOfbuflgWnxHz8bU35vc', '2019-05-07 13:15:46', '2019-05-07 13:15:46'),
(3, 'paulo', 'ayukpaul1@gmail.com', NULL, '$2y$10$VESSRAJnAL7ace3b1e0OXePk4/Rosef4VbUVj3CEbzBIe4k6Dp7Vu', '6EZH9UY73qMJQ5lcpFDlpzF4SRsp1iHqV4LbUnfptsIl5e8Pk2GOH6fA2ngW', '2019-06-17 14:14:47', '2019-06-17 14:14:47'),
(4, 'john', 'yajsj@hjj.cpm', NULL, '$2y$10$I/yGc7/4ghF3uZbf8wMj3eDGVOaEplRiOWr2MVV3QTwxEhS3DZWQa', 'qcttRbhoVa0YVDJ92w08jQU8tm3uBPMQG6Ra5PeDlrY1HCzuDT0K7neZ6ET0', '2019-09-23 14:32:42', '2019-09-23 14:32:42');

-- --------------------------------------------------------

--
-- Table structure for table `workers`
--

CREATE TABLE `workers` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `detail` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `xds_credit`
--

CREATE TABLE `xds_credit` (
  `xc_id` int(11) NOT NULL,
  `xc_x_id` int(11) NOT NULL,
  `xc_user_id` int(11) NOT NULL,
  `DateAccountOpened` varchar(100) NOT NULL,
  `SubscriberName` varchar(100) NOT NULL,
  `AccountNo` varchar(100) NOT NULL,
  `SubAccountNo` varchar(100) NOT NULL,
  `IndicatorDescription` varchar(100) NOT NULL,
  `OpeningBalanceAmt` varchar(100) NOT NULL,
  `Currency` varchar(100) NOT NULL,
  `CurrentBalanceAmt` varchar(100) NOT NULL,
  `InstalmentAmount` varchar(100) NOT NULL,
  `ClosedDate` varchar(100) NOT NULL,
  `LoanDuration` varchar(100) NOT NULL,
  `RepaymentFrequency` varchar(100) NOT NULL,
  `LastUpdatedDate` varchar(100) NOT NULL,
  `PerformanceStatus` varchar(100) NOT NULL,
  `AccountStatus` varchar(100) NOT NULL,
  `xc_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `xds_fullreport`
--

CREATE TABLE `xds_fullreport` (
  `x_id` int(11) NOT NULL,
  `x_user_id` int(11) NOT NULL,
  `x_c_id` int(11) NOT NULL,
  `ConsumerID` int(11) NOT NULL,
  `SearchOutput` varchar(100) NOT NULL,
  `Reference` varchar(100) NOT NULL,
  `Header` varchar(100) NOT NULL,
  `ReferenceNo` varchar(100) NOT NULL,
  `Nationality` varchar(100) NOT NULL,
  `NationalIDNo` varchar(100) NOT NULL,
  `BankVerificationNo` varchar(100) NOT NULL,
  `PencomIDNo` varchar(100) NOT NULL,
  `otheridNo` varchar(100) NOT NULL,
  `BirthDate` varchar(100) NOT NULL,
  `Dependants` int(11) NOT NULL,
  `Gender` varchar(10) NOT NULL,
  `ResidentialAddress1` varchar(400) NOT NULL,
  `ResidentialAddress2` varchar(200) NOT NULL,
  `ResidentialAddress3` varchar(200) NOT NULL,
  `ResidentialAddress4` varchar(200) NOT NULL,
  `CellularNo` varchar(30) NOT NULL,
  `EmailAddress` varchar(50) NOT NULL,
  `PropertyOwnedType` varchar(50) NOT NULL,
  `Surname` varchar(100) NOT NULL,
  `FirstName` varchar(100) NOT NULL,
  `TotalMonthlyInstalment` varchar(100) NOT NULL,
  `TotalOutstandingdebt` varchar(100) NOT NULL,
  `TotalAccountarrear` int(11) NOT NULL,
  `Amountarrear` varchar(100) NOT NULL,
  `TotalAccounts` int(11) NOT NULL,
  `TotalAccounts1` int(11) NOT NULL,
  `TotalaccountinGodcondition` int(11) NOT NULL,
  `TotalNumberofJudgement` int(11) NOT NULL,
  `TotalJudgementAmount` int(11) NOT NULL,
  `LastJudgementDate` varchar(100) NOT NULL,
  `TotalNumberofDishonoured` int(11) NOT NULL,
  `TotalDishonouredAmount` varchar(100) NOT NULL,
  `TotalMonthlyInstalment1` varchar(100) NOT NULL,
  `TotalOutstandingdebt1` varchar(100) NOT NULL,
  `TotalAccountarrear1` int(11) NOT NULL,
  `Amountarrear1` varchar(100) NOT NULL,
  `TotalaccountinGodcondition1` int(11) NOT NULL,
  `TotalNumberofJudgement1` int(11) NOT NULL,
  `TotalJudgementAmount1` int(11) NOT NULL,
  `LastJudgementDate1` varchar(100) NOT NULL,
  `TotalNumberofDishonoured1` int(11) NOT NULL,
  `TotalDishonouredAmount1` varchar(100) NOT NULL,
  `Rating` int(11) NOT NULL,
  `NoOfHomeLoanAccountsGood` int(11) NOT NULL,
  `NoOfHomeLoanAccountsBad` int(11) NOT NULL,
  `NoOfAutoLoanccountsGood` int(11) NOT NULL,
  `NoOfAutoLoanAccountsBad` int(11) NOT NULL,
  `NoOfStudyLoanAccountsGood` int(11) NOT NULL,
  `NoOfStudyLoanAccountsBad` int(11) NOT NULL,
  `NoOfPersonalLoanAccountsGood` int(11) NOT NULL,
  `NoOfPersonalLoanAccountsBad` int(11) NOT NULL,
  `NoOfCreditCardAccountsGood` int(11) NOT NULL,
  `NoOfCreditCardAccountsBad` int(11) NOT NULL,
  `NoOfRetailAccountsGood` int(11) NOT NULL,
  `NoOfRetailAccountsBad` int(11) NOT NULL,
  `NoOfJointLoanAccountsGood` int(11) NOT NULL,
  `NoOfJointLoanAccountsBad` int(11) NOT NULL,
  `NoOfTelecomAccountsGood` int(11) NOT NULL,
  `NoOfTelecomAccountsBad` int(11) NOT NULL,
  `NoOfOtherAccountsGood` int(11) NOT NULL,
  `NoOfOtherAccountsBad` int(11) NOT NULL,
  `x_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `analyses`
--
ALTER TABLE `analyses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `banks`
--
ALTER TABLE `banks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category_keyword`
--
ALTER TABLE `category_keyword`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `crc_credit_rating`
--
ALTER TABLE `crc_credit_rating`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `crc_report`
--
ALTER TABLE `crc_report`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `creditreport`
--
ALTER TABLE `creditreport`
  ADD PRIMARY KEY (`c_id`);

--
-- Indexes for table `creditreportraw`
--
ALTER TABLE `creditreportraw`
  ADD PRIMARY KEY (`cr_id`);

--
-- Indexes for table `credit_limit`
--
ALTER TABLE `credit_limit`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `bvn_UNIQUE` (`bvn`);

--
-- Indexes for table `customer_reports`
--
ALTER TABLE `customer_reports`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jobs_queue_index` (`queue`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `statements`
--
ALTER TABLE `statements`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `statement_category`
--
ALTER TABLE `statement_category`
  ADD PRIMARY KEY (`sc_id`);

--
-- Indexes for table `statement_samples`
--
ALTER TABLE `statement_samples`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `statement_summaries`
--
ALTER TABLE `statement_summaries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `workers`
--
ALTER TABLE `workers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `xds_credit`
--
ALTER TABLE `xds_credit`
  ADD PRIMARY KEY (`xc_id`);

--
-- Indexes for table `xds_fullreport`
--
ALTER TABLE `xds_fullreport`
  ADD PRIMARY KEY (`x_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `analyses`
--
ALTER TABLE `analyses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `banks`
--
ALTER TABLE `banks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `category_keyword`
--
ALTER TABLE `category_keyword`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `crc_credit_rating`
--
ALTER TABLE `crc_credit_rating`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `crc_report`
--
ALTER TABLE `crc_report`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `creditreport`
--
ALTER TABLE `creditreport`
  MODIFY `c_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `creditreportraw`
--
ALTER TABLE `creditreportraw`
  MODIFY `cr_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `credit_limit`
--
ALTER TABLE `credit_limit`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `customer_reports`
--
ALTER TABLE `customer_reports`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `statements`
--
ALTER TABLE `statements`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `statement_category`
--
ALTER TABLE `statement_category`
  MODIFY `sc_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `statement_samples`
--
ALTER TABLE `statement_samples`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `statement_summaries`
--
ALTER TABLE `statement_summaries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `workers`
--
ALTER TABLE `workers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `xds_credit`
--
ALTER TABLE `xds_credit`
  MODIFY `xc_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `xds_fullreport`
--
ALTER TABLE `xds_fullreport`
  MODIFY `x_id` int(11) NOT NULL AUTO_INCREMENT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
